var case_numbers = [];
var system = require('system');
var args = system.args;

var firstName = args[1] != undefined ? args[1] : '';
var lastName = args[2] != undefined ? args[2] : '';
var dob = args[3] != undefined ? args[3] : '';

getData(1);

function getData(pageNumber){
    var url = 'http://justicecourts.maricopa.gov/FindACase/caseSearchResults.asp?page='+pageNumber+'&lastName='+ lastName+'&FirstName='+firstName+'&DOB=' + dob;

    var page = require('webpage').create();
    page.open(url, function (status) {
        window.setTimeout(function () {
            var content = page.content;
            if(/no matches to your query/.test(content)){
                console.log('No matches');
                phantom.exit();
            }
            var last_page = content.match(/\?page=(\d+)&amp;.+last page/g);
            case_numbers = case_numbers.concat(content.match(new RegExp('caseNumber=(.+?)\"', 'g'))).map(function (item) {
                return item.replace('caseNumber=', '').replace('"', '');
            });
            if(last_page != null){
                last_page = parseInt(last_page[0].match(/page=(\d+)&amp/)[1]);
                if(pageNumber < last_page){
                    getData(++pageNumber);
                }
            }else{
                console.log(case_numbers);
                phantom.exit();
            }
        }, 2000);
    });
}